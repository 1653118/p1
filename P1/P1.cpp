// P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "P1.h"
#include <commctrl.h>

#define MAX_LOADSTRING     100
#define ID_TOOLBAR         2000
#define IMAGE_WIDTH        18
#define IMAGE_HEIGHT       17
#define BUTTON_WIDTH       0
#define BUTTON_HEIGHT      0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hWndMDIClient, 
	 hFrameWnd, 
	 hToolBarWnd;
int count;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    FrameWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    DrawWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    MIDCloseProc(HWND, LPARAM);
INT_PTR CALLBACK    About(HWND, LPARAM);
INT_PTR CALLBACK    Color(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Font(HWND, UINT, WPARAM, LPARAM);
//Tool bar functions
void doCreate_Toolbar(HWND hWnd);
void doToolBar_NotifyHandle(LPARAM	lParam);
void doToolBar_AddSTDButton();
void doToolBar_AddUserButton();
void doToolBar_Style(int tbStyle);
void doView_ToolBar(HWND hWnd);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW  wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.lpszClassName = L"MDI_FRAME";
	wcex.lpfnWndProc = FrameWndProc;
	wcex.hInstance = hInst;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_P1));
	wcex.hbrBackground = (HBRUSH)(COLOR_APPWORKSPACE + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_P1);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.cbClsExtra = 0;  
	wcex.cbWndExtra = 0;
	if (!RegisterClassExW(&wcex))  return  FALSE;
	
	wcex.cbSize = sizeof(WNDCLASSEX);  
	wcex.lpszClassName = L"MDI_DRAW_CHILD";  
	wcex.lpfnWndProc = DrawWndProc;  
	wcex.hInstance = hInst; 
	wcex.hCursor = LoadCursor(hInst, IDC_CROSS); 
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_P1));  
	wcex.lpszMenuName = NULL;
	if  (!RegisterClassExW(&wcex))  return  FALSE;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
void updateMenuCheck(HWND hWnd, int tmp)
{
	HMENU hMenu = GetMenu(hWnd);
	int chk;
	if (GetMenuState(hMenu, tmp, MF_BYCOMMAND)& MF_CHECKED)
		chk = MF_UNCHECKED;
	else chk = MF_CHECKED;
	CheckMenuItem(hMenu, tmp, MF_BYCOMMAND | chk);
}
void unCheck(HWND hWnd, int x, int y)
{
	HMENU hMenu = GetMenu(hWnd);
	CheckMenuItem(hMenu, x, MF_BYCOMMAND | MF_UNCHECKED);
	CheckMenuItem(hMenu, y, MF_BYCOMMAND | MF_UNCHECKED);
}

LRESULT CALLBACK FrameWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case  WM_CREATE:  
		CLIENTCREATESTRUCT  ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 1);
		ccs.idFirstChild = 50001;
		hWndMDIClient = CreateWindow(L"MDICLIENT",
									 (LPCTSTR)NULL, 
									 WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL, 
									 0, 0, 0, 0, hWnd, 
									 (HMENU)NULL, hInst,
									 (LPVOID)&ccs);
		ShowWindow(hWndMDIClient, SW_SHOW);
		return  0;
	case WM_SIZE:
		UINT  w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hWndMDIClient, 0, 0, w, h, TRUE);
		return  DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
		break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		switch (wmId)
		{
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_FILE_NEW:
		{
			count++;
			TCHAR msg[256];
			wsprintf(msg, L"Noname-%d.drw", count);
			MDICREATESTRUCT mdiCreate;
			mdiCreate.szClass = L"MDI_DRAW";
			mdiCreate.szTitle = msg;
			mdiCreate.hOwner = hInst;
			mdiCreate.x = CW_USEDEFAULT;
			mdiCreate.y = CW_USEDEFAULT;
			mdiCreate.cx = CW_USEDEFAULT;
			mdiCreate.cy = CW_USEDEFAULT;
			mdiCreate.style = 0;
			mdiCreate.lParam = NULL;
			SendMessage(hWndMDIClient, WM_MDICREATE, 0, (LONG)(LPMDICREATESTRUCT)&mdiCreate);
		}
		break;
		case ID_FILE_OPEN:
			MessageBox(hWnd, L"Bạn đã chọn Open", L"Notice", MB_OK);
			break;
		case ID_FILE_SAVE:
			MessageBox(hWnd, L"Bạn đã chọn Save", L"Notice", MB_OK);
			break;
		case ID_DRAW_COLOR:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_COLOR), hWnd, Color);
			break;
		case ID_DRAW_FONT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_FONT), hWnd, Font);
			break;
		case ID_DRAW_LINE:
			updateMenuCheck(hWnd, ID_DRAW_LINE);
			unCheck(hWnd, ID_DRAW_RECTANGLE, ID_DRAW_ELLIPSE);
			break;
		case ID_DRAW_RECTANGLE:
			updateMenuCheck(hWnd, ID_DRAW_RECTANGLE);
			unCheck(hWnd, ID_DRAW_LINE, ID_DRAW_ELLIPSE);
			break;
		case ID_DRAW_ELLIPSE:
			updateMenuCheck(hWnd, ID_DRAW_ELLIPSE);
			unCheck(hWnd,ID_DRAW_LINE, ID_DRAW_RECTANGLE);
			break;
		case ID_WINDOW_TIDE:
			SendMessage(hWndMDIClient, WM_MDITILE, MDITILE_SKIPDISABLED, 0L);
			break;
		case ID_WINDOW_CASCADE:
			SendMessage(hWndMDIClient, WM_MDICASCADE, 0L, 0L);
			break;
		case ID_WINDOW_CLOSEALL:
			EnumChildWindows(hWndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
			break;
		case ID_TOOLBAR_CREATE:
			doCreate_Toolbar(hWnd);
			break;
		case ID_TOOLBAR_ADDSTDBUTTON:
			doToolBar_AddSTDButton();
			break;
		case ID_TOOLBAR_ADDUSERBUTTON:
			doToolBar_AddUserButton();
			break;
		case ID_TOOLBAR_VIEW:
			doView_ToolBar(hWnd);
			break;
		case WM_NOTIFY:
			doToolBar_NotifyHandle(lParam);
			return 0;
		default:
			return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
		}
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return  DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
	}
	return 0;
}
LRESULT CALLBACK DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case IDM_EXIT:
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
}
LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hWndMDIClient, WM_MDIDESTROY,(WPARAM) hChildWnd, 0L);
	return 1;
}
// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
INT_PTR CALLBACK Color(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCELCOLOR)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
INT_PTR CALLBACK Font(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCELFONT)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
void doCreate_Toolbar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};

	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	CheckMenuItem(GetMenu(hWnd), ID_TOOLBAR_VIEW, MF_CHECKED | MF_BYCOMMAND);
}
void doToolBar_AddSTDButton()
{
	TBBUTTON tbButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE, ID_FILE_SAVE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};

	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}
void doToolBar_AddUserButton()
{
	// define new buttons
	TBBUTTON tbButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	    { 0, ID_DRAW_LINE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
		{ 1, ID_DRAW_RECTANGLE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_ABOUT_EXIT };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;
	tbButtons[5].iBitmap += idx;

	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}
void doView_ToolBar(HWND hWnd)
{
	int vFlag = GetMenuState(GetMenu(hWnd), ID_TOOLBAR_VIEW, MF_BYCOMMAND) & MF_CHECKED;

	if (vFlag)
	{
		ShowWindow(hToolBarWnd, SW_HIDE);
		vFlag = MF_UNCHECKED;
	}
	else
	{
		ShowWindow(hToolBarWnd, SW_SHOW);
		vFlag = MF_CHECKED;
	}

	CheckMenuItem(GetMenu(hWnd), ID_TOOLBAR_VIEW, vFlag | MF_BYCOMMAND);
}
void doToolBar_NotifyHandle(LPARAM	lParam)
{
	LPTOOLTIPTEXT   lpToolTipText;
	WCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	// ToolTipText, loaded from Stringtable resource

														// lParam: address of TOOLTIPTEXT struct
	lpToolTipText = (LPTOOLTIPTEXT)lParam;

	if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
	{
		// hdr.iFrom: ID cua ToolBar button -> ID cua ToolTipText string
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);

		lpToolTipText->lpszText = szToolTipText;
	}
}
void doToolBar_Style(int tbStyle)
{
	ShowWindow(hToolBarWnd, SW_HIDE);
	SendMessage(hToolBarWnd, TB_SETSTYLE, (WPARAM)0, (LPARAM)(DWORD)tbStyle | CCS_TOP);
	ShowWindow(hToolBarWnd, SW_SHOW);
}